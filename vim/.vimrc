call plug#begin('~/.vim/plugged')

" syntax and language specific
Plug 'pangloss/vim-javascript'
Plug 'leshill/vim-json'

" colorscheme
Plug 'fcpg/vim-farout'

call plug#end()

colorscheme farout
